# Project Participation Policies

This repository contains multiple different types of policies or compliance documents of varying degrees of relevance, depending on which project it is, the nature of their own policies and the degree of my participation.


## Codes of Conduct

This is what most people will be looking for and is found in [this directory](conduct/).  Start with the [README](conduct/README.md) and then read the actual [policy](conduct/coc-policy.org).


## DECO Compliance

Because I work on a lot of security and cryptography software and because I am also located in Australia, I needed to confirm that my work is legal and in conformance with the DECO regulations.  It is, otherwise I wouldn't be writing this, but the compliance check results are being made available here.

The checks are performed by using the [Australian Department of Defence's](https://www.defence.gov.au/) [Online DSGL Tool](https://dsgl.defence.gov.au/Pages/Questionnaire.aspx) and completing the questionairre on that site.

The first compliance check was performed in late 2015 and the second a few months later in 2016.  A third compliance check was performed in August, 2018.

**Important Note:** The compliance check results may refer to military uses, exports or publications and/or dual-use supply, exports or publication.  This does not mean that my work is definitely or explicitly supplied for those purposes, just that it may be used for those purposes just as readily as it may be used by non-military or non-militant people.  In other words, the questionairre was completed conservatively in order to make sure that all points of concern were covered.

The [2015 results are here](deco/2015/Australian_DCTA_export_DECO_Questionnaire_Results.pdf) (PDF) and its detached [OpenPGP signature is here](deco/2015/Australian_DCTA_export_DECO_Questionnaire_Results.pdf.sig).

The [2016 results are here](deco/2016/Australian_DCTA_export_DECO_Questionnaire_Results.pdf) (PDF) and its detached [OpenPGP signature is here](deco/2016/Australian_DCTA_export_DECO_Questionnaire_Results.pdf.sig).

There was no compliance check in 2017.

The [2018 results are here](deco/2018/Australian_DCTA_export_DECO_Questionnaire_Results.pdf) (PDF) and its detached [OpenPGP signature is here](deco/2018/Australian_DCTA_export_DECO_Questionnaire_Results.pdf.sig).
