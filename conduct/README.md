# Project Participation Policy

My personal policy or position with regards to the agendas, policies and codes of conduct adopted and enforced amongst various projects, communities, organisations and companies.

This has become necessary due to the rise in popularity of particular codes of conduct which are inherently bigoted and harmful regarding minorities differentiated by their genetic variance.  Essentially I cannot and will never support any such policy and explicitly reject any policy which does so.


## Policy Statement and Signature

The actual policy statement is an org-mode file [located here](coc-policy.org) and digitally signed with a detached OpenPGP signature [located here](coc-policy.org.asc).


## Australian Specific Codes of Conduct

Projects adopting any of the codes of conduct which I call out here, including the Geek Feminism work and those based on it, the Community and Contributor Covenants, along with a number of variations of them should be particularly cautious if they are operating in Australia or have members or contributors who do so.  The reason being that the crafting of policies to promote either a conservative dyadic agenda or a progressive dyadic agenda is very likely illegal in Australia and has been since the beginning of 2014 when the 2013 ammendments to the _Sex Discrimination Act 1984_ commenced.

The version of the Act with those protections added [is here](https://www.legislation.gov.au/Details/C2014C00002) and the current version [is here](https://www.legislation.gov.au/Details/C2018C00499).


## Discerning Good Codes of Conduct from Bad

This is not an easy task, particularly not with the number of minority groups around and the possibility that the definitions of what may be prejudiced behaviour may itself be prejudiced against another group.

It is, however, quite possible to identify those policies which are more likely to foster that type of situation.  They are the ones which seek to define or specify which types of people are protected from any group or person not falling into one of those categories.

Whereas the good policies are those which seek to do only what the name implies; defining the acceptible conduct of anyone within their scope with regards to others.  They also don't seek to control behaviour beyond their communities or platforms, with some notable exceptions when it comes to very serious or dangerous conduct, such as assault.  These types of codes do not seek to define an acceptable or unacceptable category of person only of their behaviour.

This is why the original version of the Python Software Foundation's code was actually superior to the [Django Project's code](https://www.djangoproject.com/conduct/) and [diversity statement](https://www.djangoproject.com/diversity/).  Unfortunately the PSF has since [updated their policy](https://github.com/psf/community-code-of-conduct) with [something more](https://www.python.org/psf/conduct/) [intersex hating](https://www.python.org/community/diversity/) and in line with both the Django Project and the Contributor Covenant.

Supporters of the latter type often criticise the former for not protecting minorities, but of those two groups it's not the Python Software Foundation's policies which provide cover for prejudice and erasure which results in the mutilation and torture of infants, not to mention the continuance of eugenics based policies of aimed at perminently eliminating certain classes of genetic variations.  Which is exactly what all dyadic codes of conduct do, including the Django Project's and [numerous others](dyadic-codes.md).  In fact one of the reasons this policy repo is on GitLab is because GitHub has also adopted dyadic policies in the past and thus cannot be trusted to not suppress this statement.

Due to the increasing popularity of codes like or based on the Contributor Covenant, there are a diminishing number of active examples of better codes.  One of those which remains in the Python space is that used by Kenneth Reitz in a number of his projects and in his post, [Be Cordial or Be on Your Way](https://www.kennethreitz.org/essays/be-cordial-or-be-on-your-way).

Interphobes, of course, can't be cordial.  If they could be, then it wouldn't take that much effort to not wallow in their bigotry and inflict it on others.
